package com.example.marioperezt.pantallatactil;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener {

    TextView tvEntrada;
    TextView tvSalida;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvEntrada = (TextView) findViewById(R.id.tv_entrada);
        tvSalida = (TextView) findViewById(R.id.tv_salida);
        tvEntrada.setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        //tvSalida.append(event.toString()+"\n");
        String acciones[] = { "ACTION_DOWN", "ACTION_UP",
        "ACTION_MOVE" , "ACTION_CANCEL" , "ACTION_OUT_SIDE", "ACTION_POINTER_DOWN", "ACTION_POINTER_UP" };
        int accion = event.getAction();
        int codigoAccion = accion & MotionEvent.ACTION_MASK ;
        tvSalida.append(acciones[codigoAccion]);
        for (int i = 0; i < event.getPointerCount(); i++) {
            tvSalida.append("puntero:" + event.getPointerId(i) +
                    " x:" + event.getX(i) + " y: " + event.getY(i));
        }
        tvSalida.append("/n");
        return false;
    }
}
